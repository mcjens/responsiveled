//define pins for the red, green and blue LEDs
#define RED_LED 6
#define BLUE_LED 5
#define GREEN_LED 8

//overall brightness value
int brightness = 255;
//individual brightness values for the red, green and blue LEDs
int gBright = 0; 
int rBright = 0;
int bBright = 0;

int blinks = 0;

int fadeSpeed = 10;


void setup() {
  //set up pins to output.
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(BLUE_LED, OUTPUT);


}



void TurnOn(){
   
    for (int i=0;i<256; i++){
      
      analogWrite(RED_LED, rBright);
      rBright +=1;
      delay(fadeSpeed);

    }
    
    for (int i=0;i<256; i++){
      
      analogWrite(BLUE_LED, bBright);
      bBright += 1;
delay(fadeSpeed);
    }  

    for (int i=0;i<256; i++){
      
      analogWrite(GREEN_LED, gBright);
      gBright +=1;
      delay(fadeSpeed);
    }  
}

void TurnOff(){
    for (int i=0;i<256; i++){
          analogWrite(GREEN_LED, brightness);
          analogWrite(RED_LED, brightness);
          analogWrite(BLUE_LED, brightness);
    
      
      brightness -= 1;
 
      delay(fadeSpeed);

    }
}


void redLight(){
  analogWrite(GREEN_LED, 0);
  analogWrite(RED_LED, 255);
  analogWrite(BLUE_LED, 0);
  
}

void blueLight(){
  analogWrite(GREEN_LED, 0);
  analogWrite(RED_LED, 0);
  analogWrite(BLUE_LED, 255);
}

void greenLight(){
  analogWrite(GREEN_LED, 255);
  analogWrite(RED_LED, 0);
  analogWrite(BLUE_LED, 0);
}

void whiteLight(){
  analogWrite(GREEN_LED, 255);
  analogWrite(RED_LED, 255);
  analogWrite(BLUE_LED, 0);
}

void orangeLight(){
  analogWrite(GREEN_LED, 250);
  analogWrite(RED_LED, 132);
  analogWrite(BLUE_LED, 5);
}

void pinkLight(){
  analogWrite(GREEN_LED, 252);
  analogWrite(RED_LED, 135);
  analogWrite(BLUE_LED, 206);
}

void mintLight(){
  analogWrite(GREEN_LED, 121);
  analogWrite(RED_LED, 247);
  analogWrite(BLUE_LED, 206);
}

void purpleLight(){
  analogWrite(GREEN_LED, 151);
  analogWrite(RED_LED, 67);
  analogWrite(BLUE_LED, 195);
}

void yellowLight(){
  analogWrite(GREEN_LED, 255);
  analogWrite(RED_LED, 239);
  analogWrite(BLUE_LED, 5);
}


void runColors(){
  redLight();
  delay(90000);
  whiteLight();
  delay(90000);
  blueLight();
  delay(90000);
  greenLight();
  delay(90000);
}

////this function will make the LED dim once the Parallax Sound Impact Sensor sends a 1 signal, and then return to it’s original brightness.
void loop()
{

if(blinks <= 25) {
  blueLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 255);
         analogWrite(BLUE_LED, 0);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 25 && blinks <= 50) {
  greenLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 0);
         analogWrite(RED_LED, 255);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 50 && blinks <= 75) {
  redLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 252);
         analogWrite(RED_LED, 135);
         analogWrite(BLUE_LED, 206);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 75 && blinks <= 100) {
  pinkLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 250);
         analogWrite(RED_LED, 132);
         analogWrite(BLUE_LED, 5);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 100 && blinks <= 125) {
  orangeLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 151);
         analogWrite(RED_LED, 67);
         analogWrite(BLUE_LED, 195);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 125 && blinks <= 150) {
  purpleLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 240);
         analogWrite(RED_LED, 142);
         analogWrite(BLUE_LED, 0);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 150 && blinks <= 175) {
  mintLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 249);
         analogWrite(RED_LED, 241);
         analogWrite(BLUE_LED, 16);
         delay(50);
         blinks += 1;
  }
}


if(blinks > 175 && blinks <= 200) {
  yellowLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(GREEN_LED, 255);
         analogWrite(RED_LED, 255);
         analogWrite(BLUE_LED, 0);
         delay(50);
         blinks += 1;
  }
}

if(blinks > 200 && blinks <= 225) {
  whiteLight();
  boolean soundstate = digitalRead(7);
  if (soundstate == 1) {
         analogWrite(BLUE_LED, 255);
         delay(50);
         blinks += 1;
  }

  if(blinks > 225){
    blinks = 0;
  }
}


}

}
